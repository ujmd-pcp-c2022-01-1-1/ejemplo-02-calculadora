﻿
namespace Callculadora_PCP
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtOperando1 = new System.Windows.Forms.TextBox();
            this.txtOperando2 = new System.Windows.Forms.TextBox();
            this.rbtnSuma = new System.Windows.Forms.RadioButton();
            this.rbtnResta = new System.Windows.Forms.RadioButton();
            this.rbtnMultiplicación = new System.Windows.Forms.RadioButton();
            this.rbtnDivisión = new System.Windows.Forms.RadioButton();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.txtResultado = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOperando1
            // 
            this.txtOperando1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtOperando1.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOperando1.Location = new System.Drawing.Point(4, 4);
            this.txtOperando1.Name = "txtOperando1";
            this.txtOperando1.Size = new System.Drawing.Size(102, 23);
            this.txtOperando1.TabIndex = 0;
            this.txtOperando1.Text = "0";
            this.txtOperando1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOperando1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOperando1_KeyPress);
            // 
            // txtOperando2
            // 
            this.txtOperando2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtOperando2.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOperando2.Location = new System.Drawing.Point(4, 4);
            this.txtOperando2.Name = "txtOperando2";
            this.txtOperando2.Size = new System.Drawing.Size(102, 23);
            this.txtOperando2.TabIndex = 1;
            this.txtOperando2.Text = "0";
            this.txtOperando2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOperando2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOperando2_KeyPress);
            // 
            // rbtnSuma
            // 
            this.rbtnSuma.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnSuma.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.rbtnSuma.Checked = true;
            this.rbtnSuma.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.rbtnSuma.FlatAppearance.BorderSize = 2;
            this.rbtnSuma.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gold;
            this.rbtnSuma.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Khaki;
            this.rbtnSuma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnSuma.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnSuma.Location = new System.Drawing.Point(128, 16);
            this.rbtnSuma.Name = "rbtnSuma";
            this.rbtnSuma.Size = new System.Drawing.Size(30, 30);
            this.rbtnSuma.TabIndex = 2;
            this.rbtnSuma.TabStop = true;
            this.rbtnSuma.Text = "+";
            this.rbtnSuma.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtnSuma.UseVisualStyleBackColor = false;
            // 
            // rbtnResta
            // 
            this.rbtnResta.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnResta.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.rbtnResta.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.rbtnResta.FlatAppearance.BorderSize = 2;
            this.rbtnResta.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gold;
            this.rbtnResta.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Khaki;
            this.rbtnResta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnResta.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnResta.Location = new System.Drawing.Point(164, 16);
            this.rbtnResta.Name = "rbtnResta";
            this.rbtnResta.Size = new System.Drawing.Size(30, 30);
            this.rbtnResta.TabIndex = 3;
            this.rbtnResta.Text = "−";
            this.rbtnResta.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtnResta.UseVisualStyleBackColor = false;
            // 
            // rbtnMultiplicación
            // 
            this.rbtnMultiplicación.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnMultiplicación.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.rbtnMultiplicación.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.rbtnMultiplicación.FlatAppearance.BorderSize = 2;
            this.rbtnMultiplicación.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gold;
            this.rbtnMultiplicación.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Khaki;
            this.rbtnMultiplicación.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnMultiplicación.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnMultiplicación.Location = new System.Drawing.Point(128, 52);
            this.rbtnMultiplicación.Name = "rbtnMultiplicación";
            this.rbtnMultiplicación.Size = new System.Drawing.Size(30, 30);
            this.rbtnMultiplicación.TabIndex = 4;
            this.rbtnMultiplicación.Text = "×";
            this.rbtnMultiplicación.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtnMultiplicación.UseVisualStyleBackColor = false;
            // 
            // rbtnDivisión
            // 
            this.rbtnDivisión.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnDivisión.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.rbtnDivisión.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.rbtnDivisión.FlatAppearance.BorderSize = 2;
            this.rbtnDivisión.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gold;
            this.rbtnDivisión.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Khaki;
            this.rbtnDivisión.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnDivisión.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnDivisión.Location = new System.Drawing.Point(164, 52);
            this.rbtnDivisión.Name = "rbtnDivisión";
            this.rbtnDivisión.Size = new System.Drawing.Size(30, 30);
            this.rbtnDivisión.TabIndex = 5;
            this.rbtnDivisión.Text = "÷";
            this.rbtnDivisión.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtnDivisión.UseVisualStyleBackColor = false;
            // 
            // btnCalcular
            // 
            this.btnCalcular.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.btnCalcular.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnCalcular.FlatAppearance.BorderSize = 2;
            this.btnCalcular.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gold;
            this.btnCalcular.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Khaki;
            this.btnCalcular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCalcular.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalcular.Location = new System.Drawing.Point(319, 29);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(30, 30);
            this.btnCalcular.TabIndex = 6;
            this.btnCalcular.Text = "=";
            this.btnCalcular.UseVisualStyleBackColor = false;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // txtResultado
            // 
            this.txtResultado.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtResultado.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResultado.Location = new System.Drawing.Point(4, 4);
            this.txtResultado.Name = "txtResultado";
            this.txtResultado.ReadOnly = true;
            this.txtResultado.Size = new System.Drawing.Size(102, 23);
            this.txtResultado.TabIndex = 7;
            this.txtResultado.Text = "0";
            this.txtResultado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.panel1.Controls.Add(this.txtOperando1);
            this.panel1.Location = new System.Drawing.Point(12, 29);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(110, 31);
            this.panel1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.panel2.Controls.Add(this.txtOperando2);
            this.panel2.Location = new System.Drawing.Point(202, 29);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(110, 31);
            this.panel2.TabIndex = 4;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.panel3.Controls.Add(this.txtResultado);
            this.panel3.Location = new System.Drawing.Point(356, 29);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(110, 31);
            this.panel3.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LemonChiffon;
            this.ClientSize = new System.Drawing.Size(480, 94);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.rbtnDivisión);
            this.Controls.Add(this.rbtnMultiplicación);
            this.Controls.Add(this.rbtnResta);
            this.Controls.Add(this.rbtnSuma);
            this.MaximumSize = new System.Drawing.Size(496, 133);
            this.MinimumSize = new System.Drawing.Size(496, 133);
            this.Name = "Form1";
            this.Text = "Calculadora";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtOperando1;
        private System.Windows.Forms.TextBox txtOperando2;
        private System.Windows.Forms.RadioButton rbtnSuma;
        private System.Windows.Forms.RadioButton rbtnResta;
        private System.Windows.Forms.RadioButton rbtnMultiplicación;
        private System.Windows.Forms.RadioButton rbtnDivisión;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.TextBox txtResultado;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
    }
}

